class Hamming
  def self.compute(first, second)
    return 0 if first == second
    raise ArgumentError if first.length != second.length
    first.chars.zip(second.chars).count { |x, y| x != y }
  end
end

module BookKeeping
  VERSION = 3
end
