class Bob
  def self.hey(remark)
    if shout?(remark) && remark.end_with?('?')
      return "Calm down, I know what I'm doing!"
    end
    return 'Whoa, chill out!' if shout? remark
    return 'Sure.' if remark.strip.end_with?('?')
    return 'Fine. Be that way!' if remark.strip.empty?
    'Whatever.'
  end
end

def shout?(string)
  string == string.upcase && string.count('a-zA-Z') > 0
end

module BookKeeping
  VERSION = 2
end
