<?php

class Robot
{
    public static $robotNames = [];

    protected $name;

    public function __construct()
    {
        $this->reset();
    }

    public static function robotNameExists($name)
    {
        return array_search($name, self::$robotNames);
    }

    public static function addRobotName($name)
    {
        array_push(self::$robotNames, $name);
    }

    public static function deleteRobotName($name)
    {
        array_push(self::$robotNames, $name);
    }

    public function getName()
    {
        return $this->name;
    }

    public function reset()
    {
        do {
            $this->name = $this->createStringSectionOfName() . $this->createNumberSectionOfName();
        } while (self::robotNameExists($this->name));
        self::addRobotName($this->name);
    }

    protected function createStringSectionOfName()
    {
        return random_uppercase_letter() . random_uppercase_letter();
    }

    protected function createNumberSectionOfName()
    {
        return mt_rand(100, 999);
    }
}

function random_uppercase_letter()
{
    $characters = range('A', 'Z');
    $max = count($characters) - 1;
    $rand = mt_rand(0, $max);
    return $characters[$rand];
}
