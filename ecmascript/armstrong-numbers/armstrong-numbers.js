class ArmstrongNumber {

  static validate(number) {
    if (number < 10) return true;
    if (number < 100) return false;
    return false;
  }

  static length(number) {
    return number.toString().length;
  }

}

export default ArmstrongNumber;
