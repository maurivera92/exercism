class Transcriptor {

  toRna(strand) {
    if (strand.length === 0) return '';
    if (strand.match('[^CGAT]') !== null) {
      throw new Error('Invalid input DNA.');
    }

    return strand.split('').map((nucleotide) => {
      if (nucleotide === 'C') return 'G';
      if (nucleotide === 'G') return 'C';
      if (nucleotide === 'A') return 'U';
      return 'A';
    }).join('');
  }

}

export default Transcriptor;
