pub fn nth(n: u32) -> u32 {
    let mut number_to_check_if_prime = 0;
    let mut nth_prime = 0;
    loop {
        if !is_prime(number_to_check_if_prime) {
            number_to_check_if_prime += 1;
            continue; // loop until we find a prime number
        }
        if nth_prime == n + 1 {
            return number_to_check_if_prime;
        };
        // the number is prime, but its not the one we're looking for
        number_to_check_if_prime += 1;
        nth_prime += 1;
    }
}

fn is_prime(number: u32) -> bool {
    if number == 0 {
        return false;
    }
    if number < 4 {
        return true;
    }
    for n in 2..=(((number as f32).sqrt()) as u32) { // there has to be a better way to do this
        if number % n == 0 {
            return false;
        }
    }
    true
}
